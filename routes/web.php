<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('machine');
});

Route::get('/products', 'ProductController@index')
    ->name('products.index');

Route::post('/buy', 'ProductController@update')
    ->name('products.update');

Route::get('/balance/{wallet}/{rub?}', 'BalanceController@show')
    ->where('wallet', 'user|pending|machine')
    ->name('balance');

Route::post('/transaction/add', 'TransactionController@store')
    ->name('transactions.store');

Route::post('/transaction/givethechange', 'TransactionController@giveTheChange')
    ->name('transactions.givethechange');
