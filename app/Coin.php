<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'value', 'title',
    ];

    /**
     * Get all of the transactions for the coin.
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
