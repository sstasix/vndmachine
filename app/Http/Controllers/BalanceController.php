<?php

namespace App\Http\Controllers;

use App\Services\BalanceService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    /**
     * Show wallet balance
     *
     * @param Request $request
     * @param BalanceService $balanceService
     * @return JsonResponse
     */
    public function show(Request $request, BalanceService $balanceService)
    {
        $balance = isset($request->rub)
            ? $balanceService->getBalanceInRUB($request->wallet)
            : $balanceService->getBalanceInCoins($request->wallet);

        return response()->json($balance);
    }
}
