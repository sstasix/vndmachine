<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\BalanceService;
use App\Services\TransactionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $products = Product::all(['id', 'name', 'price', 'quantity']);

        return response()->json($products);
    }

    /**
     * Buy product
     *
     * @param Request $request
     * @param BalanceService $balanceService
     * @param TransactionService $transactionService
     * @return string
     */
    public function update(Request $request, BalanceService $balanceService, TransactionService $transactionService)
    {
        $product = Product::findOrFail($request->product_id);

        if ($product->quantity <= 0) {
            return __('products.out_of_stock');
        }

        if ($balanceService->getBalanceInRUB('pending') < $product->price) {
            return __('products.insufficient_funds');
        }

        DB::transaction(function () use ($product, $transactionService) {
            $transactionService->transferRub('pending', 'machine', $product->price);
            $product->decrement('quantity');
        });

        return __('products.success_order');
    }
}
