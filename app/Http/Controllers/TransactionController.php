<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transaction\StoreTransactionRequest;
use App\Services\BalanceService;
use App\Services\TransactionService;
use App\Transaction;

class TransactionController extends Controller
{
    /**
     * @var BalanceService
     */
    protected $balanceService;

    /**
     * @var TransactionService
     */
    protected $transactionService;

    /**
     * TransactionController constructor.
     *
     * @param BalanceService $balanceService
     * @param TransactionService $transactionService
     */
    public function __construct(BalanceService $balanceService, TransactionService $transactionService)
    {
        $this->balanceService = $balanceService;
        $this->transactionService = $transactionService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTransactionRequest $request
     * @return void
     */
    public function store(StoreTransactionRequest $request)
    {
        if ($this->balanceService->isUserHaveCoin($request->from, $request->coin_id)) {
            $this->transactionService->transferCoin($request->from, $request->to, $request->coin_id);
        }
    }

    /**
     * Give the change to user
     *
     * @return void
     */
    public function giveTheChange()
    {
        $needReturnRUB = $this->balanceService->getBalanceInRUB('pending');

        Transaction::where('wallet', 'pending')
            ->update(['wallet' => 'machine']);

        $this->transactionService->transferRub('machine', 'user', $needReturnRUB);
    }
}
