<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wallet', 'coin_id', 'coins_quantity'
    ];

    /**
     * The coin that belong to the transaction.
     */
    public function coin()
    {
        return $this->belongsTo(Coin::class);
    }
}
