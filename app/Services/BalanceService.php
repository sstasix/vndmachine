<?php

namespace App\Services;

use App\Coin;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class BalanceService
{
    /**
     * If user have need coin for purchase
     *
     * @param string $wallet
     * @param int $coin_id
     * @return bool
     */
    public function isUserHaveCoin(string $wallet, int $coin_id): bool
    {
        return Transaction::where('wallet', $wallet)->where('coin_id', $coin_id)->sum('coins_quantity') > 0;
    }

    /**
     * Get the wallet balance in rubles
     *
     * @param string $wallet
     * @return int
     */
    public function getBalanceInRUB(string $wallet): int
    {
        $balanceInRub = 0;
        DB::table('transactions')
            ->join('coins', 'transactions.coin_id', '=', 'coins.id')
            ->selectRaw('(SUM(transactions.coins_quantity) * coins.value) as coin_balance')
            ->where('transactions.wallet', $wallet)
            ->groupBy('transactions.coin_id')
            ->get('coin_balance')
            ->each(function ($balance) use (&$balanceInRub) {
                $balanceInRub += $balance->coin_balance;
            });

        return $balanceInRub;
    }

    /**
     * Get the wallet balance in coins
     *
     * @param string $wallet
     * @return array
     */
    public function getBalanceInCoins(string $wallet): array
    {
        $coins = [];
        Transaction::select(['coins.id', 'coins.title', 'coins.value'])
            ->selectRaw('SUM(transactions.coins_quantity) as coins_sum')
            ->join('coins', 'transactions.coin_id', '=', 'coins.id')
            ->where('transactions.wallet', $wallet)
            ->groupBy('transactions.coin_id')
            ->get()
        ->each(function($transaction) use (&$coins){
            $coins[$transaction->id] = [
                'title' => $transaction->title,
                'value' => $transaction->value,
                'quantity' => $transaction->coins_sum,
            ];
        });

        return $coins;
    }
}
