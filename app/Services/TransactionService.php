<?php

namespace App\Services;

use App\Coin;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    /**
     * Transfer money between wallets
     *
     * @param string $from
     * @param string $to
     * @param int $rub
     * @return void
     */
    public function transferRub(string $from, string $to, int $rub): void
    {
        $coins = Coin::orderBy('id', 'desc')->get(['id', 'value']);
        foreach ($coins as $coin) {
            $quantity = intdiv($rub, $coin->value);
            if ($quantity) {
                $this->transferCoin($from, $to, $coin->id, $quantity);
                $rub -= $coin->value * $quantity;
            }
        }
    }

    /**
     * Transfer coins between wallets
     *
     * @param string $from
     * @param string $to
     * @param int $coin_id
     * @param int $quantity
     * @return void
     */
    public function transferCoin(string $from, string $to, int $coin_id, int $quantity = 1): void
    {
        DB::transaction(function () use ($from, $to, $coin_id, $quantity) {
            Transaction::create([
                'wallet' => $from,
                'coin_id' => $coin_id,
                'coins_quantity' => -$quantity,
            ]);

            Transaction::create([
                'wallet' => $to,
                'coin_id' => $coin_id,
                'coins_quantity' => $quantity,
            ]);
        });
    }
}
