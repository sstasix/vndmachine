<?php

return [
    'out_of_stock' => 'Нет в наличии',
    'insufficient_funds' => 'Недостаточно средств',
    'success_order' => 'Спасибо',
];
