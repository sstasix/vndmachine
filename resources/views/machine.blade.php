@extends('app')

@section('title', 'Vending Machine')

@section('content')
    <style>
        .col-md-4 button {
            display: block;
            width: 100%;
            margin: 3px;
            text-align: left;
        }

        h1 {
            text-align: center;
        }
    </style>
    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <h2>Мой кошелек</h2>
                <hr>
                <div id="wallet"></div>
            </div>
            <div class="col-md-4">
                <h2>Внесенная сумма</h2>
                <hr>
                <h1 id="pending"></h1>
                <hr>
                <button class="btn btn-outline-info" onclick="sendMoneyBack()">Сдача</button>
            </div>
            <div class="col-md-4">
                <h2>Товары</h2>
                <hr>
                <div id="goods"></div>
                <hr>
                <div id="machine-wallet"></div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            updateUserWallet();
            updateMachineWallet();
            updatePendingBalance();
            showBalanceInConsole();
            updateGoods();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).on('click', '.user-coin', function () {
                $.post('{{ route('transactions.store') }}', {
                    coin_id: $(this).attr('data-coin-id'),
                    from: 'user',
                    to: 'pending',
                })
                    .done(function (data) {
                        updateUserWallet();
                        updatePendingBalance();
                        showBalanceInConsole();
                    });
            });

            $(document).on('click', '.buy', function () {
                $.post('{{ route('products.update') }}', {
                    product_id: $(this).attr('data-product-id'),
                })
                    .done(function (data) {
                        updateUserWallet();
                        updateMachineWallet();
                        updatePendingBalance();
                        showBalanceInConsole();
                        updateGoods();
                        alert(data);
                    });
            });
        });

        function sendMoneyBack() {
            $.post('{{ route('transactions.givethechange') }}', function () {
                updateUserWallet();
                updateMachineWallet();
                updatePendingBalance();
                showBalanceInConsole();
            });
        }

        function showBalanceInConsole() {
            console.clear();
            $.get('{{ route('balance', ['wallet' => 'pending', 'rub' => 'rub']) }}', function (rub) {
                console.log('Pending balance: ' + rub);
            });

            $.get('{{ route('balance', ['wallet' => 'user', 'rub' => 'rub']) }}', function (rub) {
                console.log('User balance: ' + rub);
            });

            $.get('{{ route('balance', ['wallet' => 'machine', 'rub' => 'rub']) }}', function (rub) {
                console.log('Machine balance: ' + rub);
            });
        }

        function updatePendingBalance() {
            $.get('{{ route('balance', ['wallet' => 'pending', 'rub' => 'rub']) }}', function (rub) {
                $('#pending').text(rub + ' ₽');
            });
        }

        function updateGoods() {
            $('#goods').html('');
            $.get('{{ route('products.index') }}', function (data) {
                $.each(data, function (index, product) {
                    $('#goods').append(`
                        <button class="btn btn-info buy" data-product-id="${product.id}">
                            ${product.name} - ${product.price} ₽ <span class="badge badge-light">${product.quantity}</span>
                        </button>`);
                });
            });
        }

        function updateUserWallet() {
            $('#wallet').html('');
            $.get('{{ route('balance', 'user') }}', function (data) {
                $.each(data, function (index, coin) {
                    $('#wallet').append(`
                        <button class="btn btn-primary user-coin" data-coin-id="${index}">
                            ${coin.title} <span class="badge badge-light">${coin.quantity}</span>
                        </button>`);
                });
            });
        }

        function updateMachineWallet() {
            $('#machine-wallet').html('');
            $.get('{{ route('balance', 'machine') }}', function (data) {
                $.each(data, function (index, coin) {
                    $('#machine-wallet').append(`
                        <button class="btn btn-primary" data-coin-id="${index}">
                            ${coin.title} <span class="badge badge-light">${coin.quantity}</span>
                        </button>`);
                });
            });
        }
    </script>
@endsection

