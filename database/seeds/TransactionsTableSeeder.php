<?php

use App\Transaction;
use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::create([
            'wallet' => 'user',
            'coin_id' => 1,
            'coins_quantity' => 10,
        ]);

        Transaction::create([
            'wallet' => 'user',
            'coin_id' => 2,
            'coins_quantity' => 30,
        ]);

        Transaction::create([
            'wallet' => 'user',
            'coin_id' => 3,
            'coins_quantity' => 20,
        ]);

        Transaction::create([
            'wallet' => 'user',
            'coin_id' => 4,
            'coins_quantity' => 15,
        ]);

        Transaction::create([
            'wallet' => 'machine',
            'coin_id' => 1,
            'coins_quantity' => 100,
        ]);

        Transaction::create([
            'wallet' => 'machine',
            'coin_id' => 2,
            'coins_quantity' => 100,
        ]);

        Transaction::create([
            'wallet' => 'machine',
            'coin_id' => 3,
            'coins_quantity' => 100,
        ]);

        Transaction::create([
            'wallet' => 'machine',
            'coin_id' => 4,
            'coins_quantity' => 100,
        ]);
    }
}
