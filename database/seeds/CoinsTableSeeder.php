<?php

use App\Coin;
use Illuminate\Database\Seeder;

class CoinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coin::create([
            'id' => 1,
            'value' => 1,
            'title' => '1 рубль',
        ]);

        Coin::create([
            'id' => 2,
            'value' => 2,
            'title' => '2 рубля',
        ]);

        Coin::create([
            'id' => 3,
            'value' => 5,
            'title' => '5 рублей',
        ]);

        Coin::create([
            'id' => 4,
            'value' => 10,
            'title' => '10 рублей',
        ]);
    }
}
