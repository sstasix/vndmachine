<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Чай',
            'price' => 13,
            'quantity' => 10,
        ]);

        Product::create([
            'name' => 'Кофе',
            'price' => 18,
            'quantity' => 20,
        ]);

        Product::create([
            'name' => 'Кофе с молоком',
            'price' => 21,
            'quantity' => 20,
        ]);

        Product::create([
            'name' => 'Сок',
            'price' => 35,
            'quantity' => 15,
        ]);
    }
}
